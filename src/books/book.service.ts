import {
  BadRequestException,
  Injectable,
  Logger,
  Res,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Book } from '../entity/book.entity';
import { Person } from '../entity/person.entity';
import { Register } from '../entity/register.entity';
import { getConnection, Repository } from 'typeorm';
import { Entry } from '../entity/entry.entity';
import * as bcrypt from 'bcryptjs';
import { JwtService } from '@nestjs/jwt';
import { Response, Request } from 'express';

@Injectable()
export class BookService {
  logger: Logger;
  constructor(
    @InjectRepository(Person)
    private personRepository: Repository<Person>,
    @InjectRepository(Register)
    private registerRepository: Repository<Register>,
    @InjectRepository(Book)
    private bookRepository: Repository<Book>,
    @InjectRepository(Entry)
    private entryRepository: Repository<Entry>,
    private jwtService: JwtService,
  ) {
    this.logger = new Logger(BookService.name);
  }
  async addUser(person: Person) {
    const adddata = new Person();
    const register = new Register();
    //  const entrydata = new Entry();

    const pass = await bcrypt.hash(person.password, 10);
    adddata.userName = register.userName = person.userName;
    adddata.phnNo = person.phnNo;
    adddata.email = person.email;
    adddata.addressline1 = person.addressline1;
    adddata.addressline2 = person.addressline2;
    adddata.state = person.state;
    adddata.country = person.country;
    adddata.pinCode = person.pinCode;
    adddata.password = register.password = pass;
    adddata.register = register;

    //await this.personRepository.save(adddata);

    /*  entrydata.userName = adddata.name=person.name;
     
     adddata.addEntry(entrydata);

     this.entryRepository.save(entrydata);*/

    this.logger.log('user added successfully');

    return await this.personRepository.save(adddata);
  }

  async findOne(data: Register, response: Response) {
    const user = await this.personRepository.findOne({
      userName: data.userName,
    });
    console.log(user);
    if (!user) {
      throw new BadRequestException('invalid credentials');
    }
    if (!(await bcrypt.compare(data.password, user.password))) {
      throw new BadRequestException('invalid ');
    }
    const jwt = await this.jwtService.signAsync({ id: user.id });
    response.cookie('jwt', jwt, { httpOnly: true });
    // return user;
    //return jwt;
    return {
      message: 'success',
    };
  }

  async findUser(request: Request) {
    try {
      const cookie = request.cookies['jwt'];

      const data = await this.jwtService.verifyAsync(cookie);

      if (!data) {
        throw new UnauthorizedException();
      }

      const user = await this.personRepository.findOne({ id: data['id'] });

      const { password, ...result } = user;

      return result;
    } catch (e) {
      throw new UnauthorizedException();
    }
  }

  async logout(@Res({ passthrough: true }) response: Response) {
    response.clearCookie('jwt');
    return {
      message: 'logout',
    };
  }

  async addBook(book: Book) {
    const bookdata = new Book();
    // const entrydata = new Entry();
    bookdata.bookName = book.bookName;
    bookdata.Author = book.Author;
    bookdata.ISBNNumber = book.ISBNNumber;
    bookdata.noofpages = book.noofpages;
    /* entrydata.bookISBN = bookdata.ISBNNumber=book.ISBNNumber;
     bookdata.addEntry(entrydata);*/
    this.logger.log('book added successfully');
    return await this.bookRepository.save(bookdata);
  }

  async addBookEntry(entry: Entry) {
    const entrydata = new Entry();

    const personResult: Person = await this.personRepository.findOne({
      userName: entry.userName,
    });

    const bookResult: Book = await this.bookRepository.findOne({
      ISBNNumber: entry.bookISBN,
    });

    entry.addEntry(personResult);

    entry.bookName = bookResult.bookName;

    entrydata.bookName = bookResult.bookName;

    entrydata.userName = entry.userName;

    entrydata.bookISBN = entry.bookISBN;

    entrydata.dateOfFinishing = entry.dateOfFinishing;

    entrydata.review = entry.review;

    entrydata.URL = entry.URL;

    this.logger.log('book entry added successfully');

    return await this.entryRepository.save(entrydata);
  }

  getAllUsers(): Promise<Person[]> {
    this.logger.log('Getting all users');
    return this.personRepository.find();
  }
  async getUserDataByUserId(id: number): Promise<Person> {
    this.logger.log('Getting  user with id');
    return await this.personRepository.findOne({ id });
  }
  getAllBooks(): Promise<Book[]> {
    this.logger.log('Getting all books');
    return this.bookRepository.find();
  }
  async getBookByBookname(bookName: string): Promise<Book> {
    this.logger.log('Getting  book with bookname');
    return await this.bookRepository.findOne(bookName);
  }
  getAllEntries(): Promise<Entry[]> {
    this.logger.log('Getting all  Book Entries');
    return this.entryRepository.find();
  }
  async getEntryByBookISBN(bookISBN: number): Promise<Entry> {
    this.logger.log('Getting Book Entry with book ISBN');
    return await this.entryRepository.findOne({ bookISBN });
  }

  async deleteUser(id: number) {
    this.logger.log('Delete user with id');
    return await this.personRepository.delete({ id });
  }

  async deleteBook(id: number) {
    this.logger.log('Delete book with id');
    return await this.bookRepository.delete({ id });
  }

  async deleteEntry(id: number) {
    this.logger.log('Delete book entry with id');
    return await this.entryRepository.delete({ id });
  }

  updateUser(id: number, data: Person) {
    const data1 = new Person();
    const data2 = new Register();
    data1.userName = data2.userName = data.userName;
    data1.phnNo = data.phnNo;
    data1.email = data.email;
    data1.addressline1 = data.addressline1;
    data1.addressline2 = data.addressline2;
    data1.state = data.state;
    data1.country = data.country;
    data1.pinCode = data.pinCode;
    data1.password = data2.password = data.password;
    this.registerRepository.update({ id: id }, data2);
    this.personRepository.update({ id: id }, data);
    this.logger.log('users details updated');
    return 'successfully updated';
  }

  updateBook(id: number, bookdata: Book) {
    const book = new Book();
    book.bookName = bookdata.bookName;
    book.Author = bookdata.Author;
    book.ISBNNumber = bookdata.ISBNNumber;
    book.noofpages = bookdata.noofpages;
    this.bookRepository.update({ id: id }, book);
    this.logger.log('updated book by id');
    return 'book details updated sucessfully';
  }

  async updateEntry(id: number, info: Entry) {
    const userData: Person = await this.personRepository.findOne({
      userName: info.userName,
    });
    const data = new Entry();
    data.userName = info.userName;
    data.bookISBN = info.bookISBN;
    data.dateOfFinishing = info.dateOfFinishing;
    data.review = info.review;
    data.URL = info.URL;
    data.addEntry(userData);
    const bookData = await this.bookRepository.findOne({
      ISBNNumber: info.bookISBN,
    });
    info.bookName = bookData.bookName;
    data.bookName = bookData.bookName;
    this.entryRepository.update({ id: id }, info);
    this.logger.log('updated book  entry by id');
    return 'Entry updated sucessfully';
  }
}
