import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Entry } from '../entity/entry.entity';
import { Book } from '../entity/book.entity';
import { Person } from '../entity/person.entity';
import { Register } from '../entity/register.entity';
import { BookController } from './book.controller';
import { BookService } from './book.service';
import { JwtModule } from '@nestjs/jwt';

@Module({
  imports: [
    TypeOrmModule.forFeature([Person, Register, Book, Entry]),
    JwtModule.register({
      secret: 'secret',
      signOptions: { expiresIn: '1hr' },
    }),
  ],
  controllers: [BookController],
  providers: [BookService],
})
export class BookModule {}
