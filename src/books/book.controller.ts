import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Req,
  Res,
} from '@nestjs/common';
import { Entry } from '../entity/entry.entity';
import { Book } from '../entity/book.entity';
import { Person } from '../entity/person.entity';
import { BookService } from './book.service';
import { Response, Request } from 'express';
import { Register } from '../entity/register.entity';
import { customException } from 'src/custom/custom.exception';

@Controller('books')
export class BookController {
  constructor(private readonly bookService: BookService) {}

  @Post('/person')
  addUser(@Body() person: Person): Promise<Person> {
    return this.bookService.addUser(person);
  }
  @Post('/login')
  async login(
    @Body() login: Register,
    @Res({ passthrough: true }) response: Response,
  ) {
    return this.bookService.findOne(login, response);
  }

  /* @Get('/userlogin')
  async user(@Req() request:Request){
    const cookie= request.cookies['jwt'];
    const data = await this.jwtService.verifyAsync(cookie);
    return cookie;
  }*/
  @Get('/userlogin')
  async user(@Req() request: Request) {
    return this.bookService.findUser(request);
  }

  @Post('/logout')
  async logout(@Res({ passthrough: true }) response: Response) {
    return this.bookService.logout(response);
  }

  @Post('/book')
  addBook(@Body() book: Book): Promise<Book> {
    return this.bookService.addBook(book);
  }

  @Post('/entry')
  addBookEntry(@Body() entry: Entry): Promise<Entry> {
    return this.bookService.addBookEntry(entry);
  }

  @Get('/users')
  getAllUsers(): Promise<Person[]> {
    return this.bookService.getAllUsers();
  }

  @Get('userId/:id')
  getUserData(@Param('id') id: number): Promise<Person> {
    return this.bookService
      .getUserDataByUserId(id)
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException('user not found', HttpStatus.NOT_FOUND);
        }
      })

      .catch(() => {
        throw new customException('user not found', HttpStatus.NOT_FOUND);
      });
  }

  @Get('/books')
  getAllBooks(): Promise<Book[]> {
    return this.bookService.getAllBooks();
  }

  @Get('bookName/:bookName')
  getBookData(@Param('bookname') bookName: string): Promise<Book> {
    return this.bookService.getBookByBookname(bookName);
  }

  @Get('/entries')
  getAllEntries(): Promise<Entry[]> {
    return this.bookService.getAllEntries();
  }

  @Get('bookISBN/:bookISBN')
  getEntryData(@Param('bookISBN') bookISBN: number): Promise<Entry> {
    return this.bookService
      .getEntryByBookISBN(bookISBN)
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException('book not found', HttpStatus.NOT_FOUND);
        }
      })

      .catch(() => {
        throw new customException('book not found', HttpStatus.NOT_FOUND);
      });
  }

  @Delete('user/:id')
  delete(@Param('id') id: number) {
    return this.bookService.deleteUser(id);
  }

  @Delete('book/:id')
  deleteBook(@Param('id') id: number) {
    return this.bookService.deleteBook(id);
  }

  @Delete('entry/:id')
  deleteEntry(@Param('id') id: number) {
    return this.bookService.deleteEntry(id);
  }

  @Put('/userId/:id')
  updateUserByUserId(@Param('id') id: number, @Body() user: Person) {
    return this.bookService.updateUser(id, user);
  }

  @Put('/book/:id')
  updateBook(@Param('id') id: number, @Body() book: Book) {
    return this.bookService.updateBook(id, book);
  }

  @Put('/entry/:id')
  updateEntry(@Param('id') id: number, @Body() entry: Entry) {
    return this.bookService.updateEntry(id, entry);
  }
}
