import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToMany,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  JoinTable,
} from 'typeorm';
import { Entry } from './entry.entity';
import { Register } from './register.entity';
@Entity()
export class Person {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: '',
  })
  @IsString()
  @Column()
  userName: string;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: '',
  })
  @IsString()
  @Column()
  phnNo: string;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: '',
  })
  @IsString()
  @Column()
  password: string;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: '',
  })
  @IsString()
  @Column()
  email: String;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: '',
  })
  @IsString()
  @Column()
  addressline1: String;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: '',
  })
  @IsString()
  @Column()
  addressline2: String;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: '',
  })
  @IsString()
  @Column()
  state: String;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: '',
  })
  @IsString()
  @Column()
  country: String;

  @ApiProperty({
    type: Number,
    description: 'The address of the user',
    default: '',
  })
  @Column()
  @IsInt()
  pinCode: Number;

  @OneToOne(() => Register, (register) => register.person, {
    cascade: true,
  })
  register: Register;

  @ManyToMany(() => Entry, (entry) => entry.person, {
    cascade: true,
    onUpdate: 'CASCADE',
  })
  @JoinTable()
  entry: Entry;
}
