import { ApiProperty } from '@nestjs/swagger';

import { IsInt, IsString } from 'class-validator';

import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { Person } from './person.entity';

@Entity()
export class Entry {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: '',
  })
  @IsString()
  @Column()
  userName: string;

  @Column()
  bookName: string;

  @ApiProperty({
    type: Number,
    description: 'The address of the user',
    default: '',
  })
  @IsInt()
  @Column()
  bookISBN: number;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: '',
  })
  @IsString()
  @Column()
  dateOfFinishing: string;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: '',
  })
  @IsString()
  @Column()
  review: string;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: '',
  })
  @IsString()
  @Column()
  URL: string;

  /* @ManyToOne(() => Person, (person) => person.entry)
  person: Person;*/
  @ManyToMany(() => Person, (person) => person.entry)
  @JoinTable()
  person: Person[];

  addEntry(person: Person) {
    if (this.person == null) {
      this.person = new Array<Person>();
    }

    this.person.push(person);
  }
}
