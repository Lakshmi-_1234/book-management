import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Book {
  @PrimaryGeneratedColumn()
  id: Number;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: '',
  })
  @IsString()
  @Column()
  bookName: string;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: '',
  })
  @IsString()
  @Column()
  Author: string;

  @ApiProperty({
    type: Number,
    description: 'The address of the user',
    default: '',
  })
  @IsInt()
  @Column()
  ISBNNumber: number;

  @ApiProperty({
    type: Number,
    description: 'The address of the user',
    default: '',
  })
  @IsInt()
  @Column()
  noofpages: number;
}
