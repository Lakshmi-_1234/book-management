import { ApiProperty } from '@nestjs/swagger';

import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Person } from './person.entity';

@Entity()
export class Register {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: '',
  })
  @Column()
  userName: string;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: '',
  })
  @Column()
  password: string;

  @OneToOne(() => Person, (person) => person.register, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn()
  person: Person;
}
