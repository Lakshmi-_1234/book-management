import { SwaggerConfig } from './swagger.interface';

export const SWAGGER_CONFIG: SwaggerConfig = {
  title: 'Book-User',

  description: 'Template',

  version: '1.0',

  tags: ['Template'],
};
