import { Controller, Get, Inject, CACHE_MANAGER } from '@nestjs/common';
import { Cache } from 'cache-manager';
import { Profile } from './shared/profile';

@Controller()
export class AppController {
  fakeModel: Profile = {
    name: 'lakshmi',
    email: 'lakshmi@gmail.com',
  };

  fakeValue = 'my name is ....';
  constructor(@Inject(CACHE_MANAGER) private cacheManager: Cache) {}

  @Get('get-string-cache')
  async getSimpleString() {
    var value = await this.cacheManager.get('my -string');
    if (value) {
      return {
        data: value,
        LoadsFrom: 'redis cache',
      };
    }
    await this.cacheManager.set('my -string', this.fakeValue, { ttl: 10 });
    return {
      data: this.fakeValue,
      LoadsFrom: 'fake database',
    };
  }

  @Get('object-cache')
  async getObject() {
    var profile = await this.cacheManager.get<Profile>('my-object');
    if (profile) {
      return {
        data: profile,
        LoadFrom: 'redis cache',
      };
    }
    await this.cacheManager.set<Profile>('my-object', this.fakeModel, {
      ttl: 20,
    });
    return {
      data: this.fakeModel,
      LoadsFrom: 'fake database',
    };
  }
}
