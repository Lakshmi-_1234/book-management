import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { SwaggerModule } from '@nestjs/swagger';
import { createDocument } from './swagger/swagger';
import { HttpExceptionFilter } from './filter/http-exception.filter';
import { ValidationPipes } from './pipes/validation.pipe';
import { ValidationExceptionFilter } from './filter/validation-exception.filter';
import * as cookieParser from 'cookie-parser';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  //app.useGlobalFilters( new HttpExceptionFilter());

  app.useGlobalPipes(new ValidationPipes());

  app.useGlobalFilters(new ValidationExceptionFilter());

  app.useGlobalPipes(new ValidationPipe({ transform: true }));

  app.setGlobalPrefix('api/v1');

  SwaggerModule.setup('api', app, createDocument(app));

  app.use(cookieParser());
  app.enableCors({
    origin: 'http://localhost:8080',
    credentials: true,
  });

  await app.listen(3004);
}
bootstrap();
